//: Playground - noun: a place where people can play

import UIKit
var str = "Hello, playground"
class Person {
    var name: String
    var age: Int
    var money: Int = 0
    
    init (_ name: String, _ age: Int) {
        self.name = name
        self.age = age
    }
}

protocol LenderBehavior:class {
    func lendMoney(borrower: BorrowerBehavior, money: Int) -> Bool
    func requestPayment()
}

protocol BorrowerBehavior {
    func askForMoney(lender: LenderBehavior, money:Int)
    func receiveMoney(lender: LenderBehavior, money: Int)
    func payMoneyBack() -> Int?
}

class Lender: Person, LenderBehavior {
    var borrowedBehavior: BorrowerBehavior?
    func lendMoney(borrower: BorrowerBehavior, money: Int) -> Bool {
        guard self.money >= money else { return false }
        self.borrowedBehavior = borrower
        self.money -= money
        
        borrower.receiveMoney(lender: self, money: money)
        return true
    }
    func requestPayment() {
        if let borrower = self.borrowedBehavior {
            if let returnMoney = borrower.payMoneyBack() {
                self.money += returnMoney
                self.borrowedBehavior = nil
            }
        }
        else
        {
            print("Sorry")
        }
    }
}

class Borrower: Person, BorrowerBehavior {
    weak var lender: LenderBehavior?
    var debt: Int = 0
    
    func receiveMoney(lender: LenderBehavior, money: Int) {
        self.lender = lender
        debt = money
        self.money += money
    }
    
    func payMoneyBack() -> Int? {
        var returnMoney:Int?
        if money >= debt {
            money -= debt
            returnMoney = debt
            debt = 0
            self.lender = nil
            return returnMoney
        }
        else {
            return nil
        }
    }
    
    func askForMoney(lender: LenderBehavior, money: Int) {
        if lender.lendMoney(borrower: self, money: money){
            print("Yeah !!")
        }
        else
        {
            
        }
    }
}

let lenderObj = Lender("Duy",10)
lenderObj.money = 2000

let borrowerObj = Borrower("Duy02",09)
borrowerObj.askForMoney(lender: lenderObj, money: 1000)

lenderObj.requestPayment()

