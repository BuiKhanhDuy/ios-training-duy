//: Playground - noun: a place where people can play

import UIKit

class Quadrat {
    var lange: Double
    {
        willSet {
            print("The new value is: \(newValue)")
        }
        didSet {
            print("The old value is: \(lange)")
        }
    }
    var flache: Double
    {
        get
        {
            return self.lange * self.lange
        }
        set
        {
            self.lange = sqrt(newValue)
        }
    }
    
    var unfang: Double {
        get
        {
        return self.lange * 4.0
        }
        set
        {
            self.lange = newValue / 4
        }
    }
    
    init (lange: Double)
    {
        self.lange = lange
    }
}

var mainQuadrat = Quadrat(lange:4.0)
mainQuadrat.unfang
mainQuadrat.lange
mainQuadrat.flache = 100.0
mainQuadrat.unfang = 64
