//: Playground - noun: a place where people can play

import UIKit

class Employee {
    
    // Create the properties with initialized value
    var id:Int = 0
    var name:String = ""
    var task:String = ""
    var salary:Float = 0.0
    var experiment:Int = 0

    // Create a void function
    func display()
    {
        // If experiment = 0, not print it out
        if experiment == 0
        {
        // Print out the result with all properties without experiment
        print("ID is: \(id), Name is: \(name), Task is: \(task), Salary is: \(salary)")
        }
        // otherwise print it out
        else
        {
        // Print out the result with all properties with experiment
        print("ID is: \(id), Name is: \(name), Task is: \(task), Salary is: \(salary), Experiment is: \(experiment)")
        }
    }
    
    // Create a Int function with "experiment" variable
    func yearOfExperiment(_ experiment:Int) -> Int
    {
       // Increase a unit
       self.experiment += experiment
        return self.experiment
    }
}
func displayAward(_ award:String) -> String
{
    // Declare an array
    var awardArray = ["Champion League", "EPL", "La Liga"]
    
    // Get String From Input
    // Compare with string in array (This array here could be champion league, C1, championleague then we should also make a lot of comparison
    if ((award.lowercased().contains("championleague")) || (award.lowercased().contains("champion league")) || (award.lowercased().contains("c1")))
    {
        //Get the value from array
        //Then print out
        return awardArray[0]
    }
    
    // Get String From Input
    // Compare with string in array
    if ((award.lowercased().contains("premierleague")) || (award.lowercased().contains("premier league")) || (award.lowercased().contains("epl")))
    {
        // Get the value from array
        // Then print out
        return awardArray[1]
    }
    
    // Get String From Input
    // Compare with string in array
    if ((award.lowercased().contains("la liga")) || (award.lowercased().contains("spainsh")) || (award.lowercased().contains("laliga")))
    {
        //Get the value from array
        //Then print out
        return awardArray[2]
    }
    
    else
    {
        // Print out the result
        return "Unknown league"
    }
}

class ManagerFootball : Employee
{
    // Declare a temporary variable
    var awardTemp:String = ""
    
    // Override the display function
    override func display() {
        // Call the previous function
        super.display()
        
        // Get the league and assign to a variable
        let string = displayAward(awardTemp)
        
        // Print the result out
        print("And he is working at: \(string)")
    }
}
func displayingResults()
{
    // Declare a object
    let employee01 = Employee()
    
    // Assign values into variable
    employee01.id = 1
    employee01.name = "Duy Bui"
    employee01.task = "Training"
    employee01.salary = 8.7
    
    // Display the result
    employee01.display()
    
    // Declare a object without assign values into variable
    let employee02 = Employee()
    
    // Display the result
    employee02.display()
    
    // Declare a object
    let employee03 = Employee()
    
    // Assign values into variable
    employee03.id = 2
    employee03.name = "Ronaldo"
    employee03.task = "Football"
    employee03.salary = 100.12
    employee03.yearOfExperiment(12)
    
    // Display the result
    employee03.display()
}
func displayResultsOfManager()
{
    // Declare a object
    let manager01 = ManagerFootball()
    
    // Assign values into variable
    manager01.id = 10
    manager01.name = "Mourinho"
    manager01.task = "Main Manager"
    manager01.salary = 120.12
    manager01.experiment = 2
    manager01.awardTemp = "LaLigA"
    
    // Display the result
    manager01.display()
    
    // Declare a object
    let manager02 = ManagerFootball()
    
    // Assign values into variable
    manager02.awardTemp = "PremierlEAGUE"
    
    // Display the result
    manager02.display()
    
    // Declare a object
    let manager03 = ManagerFootball()
    
    // Display the result
    manager03.display()
}

// Display all final results for all employees
displayingResults()

// Display all final results for all managers
displayResultsOfManager()
