//: Playground - noun: a place where people can play

import UIKit

func introduce (name: String, age:Int)
{
    if name != "" && age > 0
    {
        print("I am \(name), \(age)")
    }
}

func introduceUpgrade (_ name: String,_ age: Int)
{
    if name == "" && age <= 0 {return}
    print("I am \(name), \(age)")
}

func introduceGuard (_ name: String,_ age: Int)
{
    guard name != "" && age > 0 else {return}
    print("I am \(name), \(age)")
}
introduce(name:"Duy Bui",age:12)
introduceUpgrade("Duy BK",13)
introduceGuard("Duy BK02", 14)
// ----The next step with optional

func intro (_ name: String?,_ age: Int?)
{
    if name != nil && age != nil
    {
        if name != "" && age! > 0
        {
            name!.uppercased()
            print("I am \(name!), \(age!)")
        }
    }
}
func introBinding (_ name: String?,_ age: Int?)
{
    if let name = name, let age = age
    {
        if name != "" && age > 0
        {
            print("I am \(name), \(age)")
        }
    }
}

func introFail (_ name: String?,_ age: Int?)
{
    if name == nil || age == nil {return}
    //print("I am \(name!), \(age!)")
    
    if let name = name, let age = age {
        //
    } else { return }
    
    print("I am \(name), \(age)")
}

func introGuar (_ name: String?,_ age: Int?)
{
    // Solution 01
    //guard let name = name, let age = age else { return }
   //     print("I am \(name), \(age)")
    //}

    // Solution 02
    guard let name = name, name != "" else {return}
    guard let age = age, age > 0 else {return}

    print("I am \(name), \(age)")
}
intro("DuyBuiK",16)
introBinding("duybk", 18)
introFail("DuyTest", 19)
introGuar("Duytest02", 22)
