//: Playground - noun: a place where people can play

import Cocoa

// Declare parameters and constants
let a = 1
let b = 2
var c = 3

// Add these there numbers
let d = a + b + c

// Create a "void" method which plus two numbers
func plusTwo()
{
    // a plus b
    c = a - b
    // print out the result
    print("The result is: \(c)")
}

// Call the method/ function
plusTwo()

// Create a "Int" method which plus two numbers
func addTwo2() -> Int
{
    // a plus b
    c = a - d
    // Return the result
    return c
}

// Assign the result to a variable
var g = addTwo2()

// Print out the result
print("\(g) is the result")

// Create a "Int" method containing arguments, parameters and Types
func addTwo3(arg number1:Int, arg number2:Float) -> Int
{
    // casting and rounding a float number to the nearest integer
    // return the result
    return number1 - Int(number2.rounded())
}

// Assign the result to a parameter
var qa = addTwo3(arg: 12, arg: 2.9)

// Create a "Int" method containing arguments, parameters and Types
func addTwo4(number1:Float, arg number2: Double) -> Int
{
    // casting and rounding a double number to the nearest float
    // return the result
    return Int((number1 - Float(number2)).rounded())
}

// Create a "String" method containing arguments, parameters and Types
func display(number1:Float, arg number2: Double) ->String
{
    // take a function and assign the result to the parameter
    let g1 = addTwo4(number1: number1, arg: number2)
    
    // return the result
    return "The another result is \(g1)"
}

// Call the method and assign to a variable
var displayString = display(number1: 5.12, arg: 10.77)

// Print out the result
print(displayString)
