//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"
class Person {
    var name: String
    var age: Int
    var money: Int = 0
    
    init (_ name: String, _ age: Int) {
        self.name = name
        self.age = age
    }
}

class Lender: Person {
    var borrowerIsPointedToByLender: Borrower?
    // function đòi nợ
    func requestPayment()
    {
        if let borrowerIsPointedToByLender = self.borrowerIsPointedToByLender {
            if borrowerIsPointedToByLender.money >= borrowerIsPointedToByLender.debt {
                borrowerIsPointedToByLender.money -= borrowerIsPointedToByLender.debt
                self.money += borrowerIsPointedToByLender.debt
                borrowerIsPointedToByLender.debt = 0
            }
        }
    }
}

class Borrower: Person {
    weak var lenderIsPointedToByBorrower: Lender?
    var debt: Int = 0
    func borrowMoney(lenderTemp: Lender, money: Int) {
        if lenderTemp.money >= money {
            lenderTemp.money -= money
            self.money += money
            debt = money
            
            self.lenderIsPointedToByBorrower = lenderTemp
            lenderTemp.borrowerIsPointedToByLender = self
        }
    }
}

let lenderObj = Lender("Teo", 22)
lenderObj.money = 2000

let borrowerObj = Borrower("Ti", 18)
borrowerObj.borrowMoney(lenderTemp: lenderObj, money: 1000)

//lenderObj.requestPayment()

lenderObj
borrowerObj
