//: Playground - noun: a place where people can play

import UIKit

class User {
    var name: String
    var phones = [PhoneTest]()
    var subscriptions = [CarrierSubscription]()
    func add(_ phone: PhoneTest)
    {
        phones.append(phone)
        phone.owner = self
    }
    init(_ name: String) {
        self.name = name
        print("User \(name) is initialized")
    }
    
    deinit {
        print("User \(name) is being deallocated")
    }
}

class PhoneTest {
    let model: String
    weak var owner: User?
    var carrierSubscription: CarrierSubscription?
    func provision(carrierSubscription: CarrierSubscription) {
        self.carrierSubscription = carrierSubscription
    }
    func decommission() {
        self.carrierSubscription = nil
    }
    init(_ model: String) {
        self.model = model
        print("Phone \(model) is initialized")
    }
    deinit {
         print("Phone \(model) is being deallocated")
    }
}

class CarrierSubscription {
    let name: String
    let countryCode: String
    let number: String
    unowned var user: User
    lazy var completePhoneNumber: () -> String = { [unowned self] in
         self.countryCode + " " + self.number
    }
    init(_ name: String,_ countrySide: String,_ number: String,_ user: User) {
        self.name = name
        self.countryCode = countrySide
        self.number = number
        self.user = user
        user.subscriptions.append(self)
        print("CarrierSubscription \(name) is initialized")
    }
    deinit {
        print("CarrierSubscription \(name) is being deallocated")
    }
}

var user2:User? = User("Ronaldo")
var iPhone:PhoneTest? = PhoneTest("iPhone 6S")
user2!.add(iPhone!)
var subscription1:CarrierSubscription? = CarrierSubscription("Messi","Agren","0122",user2!)
iPhone!.provision(carrierSubscription: subscription1!)
print(subscription1!.completePhoneNumber())
user2 = nil
iPhone = nil
subscription1 = nil


