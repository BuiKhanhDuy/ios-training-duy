//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

let name = ["Ronaldo", "Messi", "Neymar", "Suarezinho"]

func backwards (s1: String, s2: String) -> Bool {
    return s1 > s2
}

var reversed = name.sorted(by: backwards)

var reveresedName = name.sorted(by: {(s1: String, s2: String) -> Bool in return s1 > s2 })

reveresedName

var reversedName02 = name.sorted(by: {(s1, s2) in return s1 > s2})

var reversedName03 = name.sorted(by: {s1, s2 in s1 > s2})

var reversedName04 = name.sorted(by: {$0 > $1})

var reversedName05 = name.sorted(by: >)

// not use trailing closure

var notUseTrailing = name.sorted(by: {(s1: String, s2: String) -> Bool in return s1 < s2})
notUseTrailing
var useTrainling = name.sorted(){(s1: String, s2: String) -> Bool in return s1 < s2}
useTrainling


// combine nested function
func buy(at place:String) -> String {
    return "This is a \(place)"
}

var s3 = {(s1:String, s2:String) -> String in return ("Buy \(s1) at" + buy(at: s2))}
s3("Tv", "FPT")

func buy(_ place: String) -> (String)->String
{
    var things = [String]()
    return {
        (_ thing:String) -> String in
        things.append(thing)
        return "Buy \(things) at \(place)"

    }
//    func buyThings (_ thing: String) -> String {
//        things.append(thing)
//        return "Buy \(things) at \(place)"
//    }
//    return buyThings(_:)
}

let buy1 = buy("Dien may xanh")
let buy2 = buy("The gioi di dong")

buy1("TV")
buy1("Lap")
buy2("iPhone")


//
func makeIncrementer (forIncrement amount: Int) -> ()->Int {
    var runningTotal = 20
    func increment() -> Int {
        runningTotal += amount
        return runningTotal
    }
    return increment
}

var pp = makeIncrementer(forIncrement: 20)
print(pp())
print(pp())
print(pp())

var pp1 = makeIncrementer(forIncrement: 10)
print(pp1())
print(pp1())
print(pp())

let alsopp = pp1
alsopp()

var testGiaTri = 0
func testFunc ()
{
    testGiaTri = 2
}

func testFunction(_ initArgument: Int, _ initDescription: String) -> (Int, String) -> String {
    let localArgument = 10;
    let localDescription = "";
    return
        {
            (_ usedInObjectArgument: Int, _ usedInObjectDescription: String) ->String in
                let allAgrument = initArgument + usedInObjectArgument + localArgument
                return initDescription + usedInObjectDescription + localDescription + "\(allAgrument)"
        }
}

var test01 = testFunction(0, "Test Of Initialization")
var stringTest01 = test01(1, "Test Of Using it")
print(stringTest01)

var test02 = test01
print(test02(2, "Test Of Second Time"))


