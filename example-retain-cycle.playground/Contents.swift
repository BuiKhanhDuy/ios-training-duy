//: Playground - noun: a place where people can play

import UIKit

class Author {
    var book:Book?
    deinit {
        print("DeAlloc_Author")
    }
}

class Book {
    weak var author:Author?
    deinit {
        print("DeAlloc_Book")
    }
}

var authorObj:Author? = Author() // return count = 1 for Author because authorObj is strong
authorObj!.book = Book() // return count = 1 for Book because authorObj!.book is strong as well
authorObj!.book?.author = authorObj // create another author reference and assign it to first author, return count 2

authorObj = nil // if we assign it back to nil, Author memory cell is currently 1, because it still has "authorObj!.book!.author" referring to it

func getIncrement(step:Int) -> ()->Int {
    var number = 0
    func handleIncrement() -> Int {
        number += step
        return number
    }
    return handleIncrement
}
//
let increase = getIncrement(step: 10)
increase()
increase()

let increase2 = increase
increase2()
