//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"
//1. Extend a Computed Properties
extension Double {
    var meter: Double {
        return self
    }
    var kilometer: Double {
        return self*1000.0
    }
    var centimeter: Double {
        return self/100.0
    }
    var milimeter: Double {
        return self/1000.0
    }
}
//. dot to call the property of 192.0 number for example
let aDistance = 42.0.meter + 192.0.kilometer + 1233.0.centimeter + 12.32.milimeter
print(aDistance)


//2. Extend Initializers
struct Point {
    var x: Double
    var y: Double
    // Instead of doing initialization in here
    // We can do it in extension function
//    init (xValue: Double, yValue: Double)
//    {
//        self.x = xValue
//        self.y = yValue
//    }
}
extension Point {
    init (xValue: Double, yValue: Double)
    {
        self.x = xValue
        self.y = yValue
    }
}
let myPoint = Point(xValue: 12.2, yValue: 0.8)

//3. Extend a static method
extension UIColor {
    static func rgba (_ red: CGFloat,_ green: CGFloat,_ blue: CGFloat,_ alpha: CGFloat) -> UIColor {
        return UIColor(red: red/255.0, green: green/255.0, blue: blue/255.0, alpha:alpha)
    }
}
let myColor = UIColor.rgba(250, 128, 144, 1)
print("myColor = \(myColor)")

//--
struct Rectangle {
    var width, height: Double
}
//4. Extend an instance method
extension Rectangle {
    func calculateArea() -> Double {
        return self.width * self.height
    }
}

var rec = Rectangle(width: 12, height: 10)
print("the result is \(rec.calculateArea())")

//Extend mutating Instance Methods
extension Double {
    mutating func square() {
            self = self*self //mutating method which method that change "self"
    }
}
var someDouble = 10.0
someDouble.square()




