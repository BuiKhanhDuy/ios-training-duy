//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"
// Initialize an empty array and inside it is double type values
var someDoubles = [Double]()
someDoubles.count
someDoubles.append(100.12)
someDoubles.append(12.12)
print(someDoubles.count)

// Initialize an array which repeats 5 number 10 times
var tenIntegers = Array(repeating: 5, count: 10)
print(tenIntegers)

// Similar one
var twoIntegers = Array(repeating: 3, count: 2)
print(twoIntegers)

// Add two arrays
var result1 = tenIntegers + twoIntegers
print(result1)

// Another way to initialize an array:
var smartPhones = ["iPhone", "Samsung", "HTC"]
print(smartPhones)

// += [] is similar to append as above. However, we see that it could insert many elements, not only one
smartPhones += ["fixed Phone"]
print(smartPhones)

smartPhones += ["nokia", "blackberry"]
print(smartPhones)

// Two access a couple of elements, use: array[n...m] or array[n..<m]
print(smartPhones[1...4])
print(smartPhones[1..<4])

// Insert a value into an index
smartPhones.insert("oppo", at: 3)
print(smartPhones)

// Remove a value basing on its index
smartPhones.remove(at: 1)
print(smartPhones)

// Iterate an array
for smartPhone in smartPhones {
    print("\(smartPhone)")
}

for (index, value) in smartPhones.enumerated() {
    print("\(index) and \(value)")
}
