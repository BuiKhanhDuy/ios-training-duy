//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

func swap2Ints(_ a: inout Int,_ b: inout Int) {
    let temp = a
    a = b
    b = temp
}

var x = 33
var y = 44
print("After: x = \(x) and y = \(y)")
swap2Ints(&x, &y)
print("After: x = \(x) and y = \(y)")
// Generic Type
func swap2Values<T>(_ a: inout T,_ b: inout T) {
    let temp = a
    a = b
    b = temp
}
var str1 = "This is first string"
var str2 = "This is second string"
print("After: string1 = \(str1) and string2 = \(str2)")
swap2Values(&str1, &str2)
print("After: string1 = \(str1) and string2 = \(str2)")

//Define Stack struct with type "Element" or "T"
struct Stack<T> {
    var items = [T]()
    mutating func push (_ item: T) {
        items.append(item)
    }
    mutating func pop() -> T? {
        return items.count == 0 ? nil : items.removeLast()
    }
}

var integerStack = Stack<Int>()
integerStack.push(1)
integerStack.push(2)
integerStack.push(3)
integerStack.push(4)

print("integerStack = \(integerStack)")
print("\(integerStack.pop()!)")
print("integerStack = \(integerStack)")

// Type Constraint
func findIndex<T: Equatable>(of valueToFind: T, in array:[T]) -> Int? {
    for (index, value) in array.enumerated() {
        if value == valueToFind {
            return index
        }
    }
    return nil
}
let intIndex = findIndex(of: 1, in: [1,2,3])
let stringIndex = findIndex(of: "Luan", in: ["Duy", "Luan", "Huy"])

// Associated Types

protocol Container {
    associatedtype T
    mutating func append(_ item: T)
    var count: Int { get }
}

struct IntStack: Container {
    // original IntStack implementation
    var items = [Int]()
    mutating func push (_ item: Int) {
        items.append(item)
    }
    mutating func pop() -> Int {
        return items.removeLast()
    }
    
    // conformance to the Container protocol
    typealias T = Int
    mutating func append(_ item: Int) {
        self.push(item)
    }
    var count: Int {
        return items.count
    }
}
