//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"
// Every single case
enum Gender {
    case Boy
    case Girl
    case Homo
}

var myGender: Gender = Gender.Girl

switch myGender {
case Gender.Boy:
    print("I'm Boy")
case .Girl:
    print("I'm Girl")
default:
    print ("I'm Nothing")
}
//-------
// Many of case in one line
enum DateOfWeek {
    case Monday, Tuesday, Wednesday, Thursday, Friday
}

var myDate: DateOfWeek = DateOfWeek.Tuesday

switch myDate {
case .Monday:
    print("This is Monday")
case .Tuesday, .Wednesday:
    print ("This is maybe Tue or Wed")
case .Friday:
    print ("This is Friday")
default:
    print("Wrong")
}

// Associated value
enum Answer {
    case Yes
    case No(Int, String, Double)
}

var ans:Answer = .Yes

switch ans {
case .Yes:
    print("Nothing to do")
case .No(let a, let b, let c):
    print(a,b,c)
default:
    print("No")
}

ans = Answer.No(12, "No", 1.23)

// Raw value
enum TestStringAndRawValueInt: Int {
    case One = 1 , Two, Three, Four, Five
}
var test:TestStringAndRawValueInt = TestStringAndRawValueInt(rawValue: 2)!
switch test {
case .One:
    print("One")
case .Two:
    print("Two")
case .Three:
    print("Three")
default:
    print("Nothing")
}
