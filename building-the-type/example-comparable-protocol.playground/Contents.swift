//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

class Teacher{
    var age: Int
    init (age: Int) {
        self.age = age
    }
}
extension Teacher: Comparable {
    static func == (lhs: Teacher, rhs: Teacher) -> Bool {
        return lhs.age == rhs.age
    }
    static func < (lhs: Teacher, rhs: Teacher) -> Bool {
    if (lhs.age < rhs.age)
    {
        return true
    }
    else {
        return false
        }
    }
}

var t1 = Teacher(age: 20)
var t2 = Teacher(age: 21)
print ("\(t1 > t2)")
print ("\(t1 != t2)")
