//: Playground - noun: a place where people can play

import UIKit

protocol DetailInformationProtocol {
    var fullName: String { get }
    func showDetail() -> String
}

class User: DetailInformationProtocol {
    var firstName: String
    var lastName: String
    init(firstName: String, lastName: String) {
        self.firstName = firstName
        self.lastName = lastName
    }
    
    var fullName: String {
        return lastName + firstName
    }
    
    func showDetail() -> String {
        return "Nothing here"
    }
}
protocol ToggleProtocol {
    //Mutating Method Requirements
    mutating func toggle()
}

enum Switcher: ToggleProtocol {
    case on, off
    mutating func toggle() {
        switch self {
        case .off:
            self = .on
        case .on:
            self = .off
        }
    }
}

var lightSwitch = Switcher.off
lightSwitch.toggle()
lightSwitch.toggle()

// Initializer Requirements
protocol InitializationProtocol {
    init(name: String)
    //TODO
}
class Animal {
    //TODO
}
class Bird: Animal, InitializationProtocol {
    var name: String
     required init(name: String) {
        self.name = name
    }
}
let myBird = Bird(name: "Duy")

