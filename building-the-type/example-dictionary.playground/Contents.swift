//: Playground - noun: a place where people can play

import UIKit
//1. Declare a dictionary - the explicit way
var user:[String: String] = ["name": "duy", "email": "khanh.duy29771@gmail.com"]
var numbers:[Int: String] = [1: "duy", 2: "bui", 3:"khanh"]

//2. Access & call & insert & update & remove an implement
  // if knowing the key
  // Access: be noticed that because the value
print(numbers[1]!)
user.updateValue("01227552338", forKey: "sdt")
numbers.updateValue("khanh.duy", forKey: 4)
numbers
numbers.updateValue("duyiOS", forKey: 1)
numbers
numbers.removeValue(forKey: 3)
numbers

//3. Delele all values
user = [:]
user.isEmpty

//4. Iterate the dictionary
for (key, value) in numbers {
    print("\(key) = \(value)")
}

//5. Iterate only key or value
for eachKey in numbers.keys {
    print("each key = \(eachKey)")
}

for eachValue in numbers.values {
    print("each value = \(eachValue)")
}

//6. Assign array of keys/ values for an array
let allKeys = [Int](numbers.keys)
print(allKeys)
let allValues = [String](numbers.values)
print(allValues)
