//: Playground - noun: a place where people can play

import UIKit

protocol ExampleDelegate {
    func method1()
}
class ExampleClass {
    var delegate: ExampleDelegate?
    func reLoadData() {
        print("To Do Reload")
        delegate?.method1()
        print("To Do Finish")
    }
}
class ClassConformDelegate: ExampleDelegate {
    var aProperty: ExampleClass
    init (aProperty: ExampleClass) {
        self.aProperty = aProperty
        self.aProperty.delegate = self
        self.aProperty.reLoadData()
    }
    func method1() {
        print("ClassConfromDelegate calls method1")
    }
}
let a = ClassConformDelegate(aProperty: ExampleClass())
