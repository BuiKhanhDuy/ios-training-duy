//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

@objc protocol CodingTask
{
    @objc optional func codingFeature01()
    @objc optional func codingFeature02() -> Int
    @objc optional func codingFeature03() -> String
}
@objc protocol TestingTask
{
    @objc optional func testingFeature01()
    @objc optional func testingFeature02() -> Int
    @objc optional func testingFeature03() -> String
}
@objc protocol BATask
{
    @objc optional func receivingRequirement()
    @objc optional func estimateDays() -> Int
    @objc optional func results() -> String
}
class TechnicalLead {
    var delegateCode: CodingTask?
    var delegateTest: TestingTask?
    var delegateBA: BATask?
//    func inforWholeProject() {
//        print("---Kick off the project---")
//        delegateBA?.receivingRequirement!()
//        var days = delegateBA?.estimateDays!()
//        print("---Meeting and confirm with the client---")
//        print("---Start to code----")
//        delegateCode?.codingFeature01!()
//        var codeFeature03 = delegateCode?.codingFeature03!()
//        print("---Make the full regression---")
//        delegateTest?.testingFeature01!()
//        delegateTest?.testingFeature01!()
//    }
}

class CoderPHP: CodingTask {
//    var contactTL: TechnicalLead
//    init (contactTL: TechnicalLead)
//    {
//        self.contactTL = contactTL
//        self.contactTL.delegateCode = self
//        self.contactTL.inforWholeProject()
//    }
    func codingFeature01() {
        print ("Starting f01 --> In progress 01 --> Finished feature 01")
    }
    func codingFeature02() -> Int {
        print("Starting f02 --> In progress 02 --> Finished feature 02")
        return 2
    }
//    func codingFeature03() -> String {
//        print("Starting f03 --> In progress 03 --> Finished feature 03")
//        return "This is f03"
//    }
}

class CoderSwift: CodingTask {
//    var contactTL: TechnicalLead
//    init (contactTL: TechnicalLead)
//    {
//        self.contactTL = contactTL
//        self.contactTL.delegateCode = self
//        self.contactTL.inforWholeProject()
//    }
//    func codingFeature01() {
//        print ("Starting f01 --> In progress 01 --> Finished feature 01")
//    }
//    func codingFeature02() -> Int {
//        print("Starting f02 --> In progress 02 --> Finished feature 02")
//        return 2
//    }
    func codingFeature03() -> String {
        print("Starting f03 --> In progress 03 --> Finished feature 03")
        return "This is f03"
    }
}

class BAAndTesterGroup: BATask, TestingTask {
//    var contactTL: TechnicalLead
//    init (contactTL: TechnicalLead)
//    {
//        self.contactTL = contactTL
//        self.contactTL.delegateBA = self
//        self.contactTL.delegateTest = self
//        self.contactTL.inforWholeProject()
//    }
    func receivingRequirement()
    {
        print("This is requirement of clients")
    }
    func estimateDays() -> Int {
        print("This is estimation time: ")
        return 10
    }
    func results() -> String {
        print("This is final report of our project")
        return "Done"
    }
    func testingFeature01() {
        print ("Testing f01 --> In progress 01 --> Done feature 01")
    }
    func testingFeature02() -> Int {
        print("Testing f02 --> In progress 02 --> Done feature 02")
        return 1
    }
    func testingFeature03() -> String {
        print("Testing f03 --> In progress 03 --> Done feature 03")
        return "Be able to release"
    }
}

let technicalLead = TechnicalLead()
let coder01 = CoderPHP()
let coder02 = CoderSwift()
let ba = BAAndTesterGroup()
let tester = BAAndTesterGroup()
print("---Kick off the project---")
technicalLead.delegateBA = ba
technicalLead.delegateBA?.receivingRequirement!()
technicalLead.delegateCode = coder01
technicalLead.delegateCode?.codingFeature01!()
technicalLead.delegateCode = coder02
technicalLead.delegateCode?.codingFeature03!()
technicalLead.delegateTest = tester
print(technicalLead.delegateTest!.testingFeature03!())


