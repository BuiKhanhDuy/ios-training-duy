//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

//var cache = NSCache<AnyObject, AnyObject>()
var set = NSCountedSet()
set.add("Duy")
set.add("Luan")
set.add("Huy")
set.add("Duy")
set.add("Huy")
set.add("Duy")
print(set)
print(set.count(for: "Duy"))
var set2 = [String]()
set2.append("Duy")
set2.append("Duy")
set2.append("Luan")
set2.append("Huy")
set2.append("Duy")
set2.append("Huy")
set2.append("Duy")
print(set2)

class DuyObject {
    //TODO
}
let cache = NSCache<NSString, DuyObject>()
let myObject: DuyObject
if let cachedThing = cache.object(forKey: "CachedObject") {
    myObject = cachedThing
}
else
{
    myObject = DuyObject()
    cache.setObject(myObject, forKey: "CachedObject")
}

let cache02 = NSCache<NSString, DuyObject>()
let duy01 = DuyObject()
let duy02 = DuyObject()
cache02.setObject(duy01, forKey: "Duy 01")
cache02.setObject(duy02, forKey: "Duy 02")
print(cache02)


