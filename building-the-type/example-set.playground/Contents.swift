//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"
//1. Set likes an array that we do not care about its sort order, unorderly mess
var starWarCharacters: Set<String> = ["Duy", "Luan", "Huy"]
if (starWarCharacters.isEmpty) {
    print("No characters")
}
else
{
    print("There are \(starWarCharacters.count)")
}

//2. Insert: When inserting, the new implement will be put at random place (depends on how it's sorted). Not like the way which array does-put at last slot
print(starWarCharacters)
starWarCharacters.insert("Asnet")
print(starWarCharacters)

//3. Remove: starWarCharacters.remove("Luan") will return the removed object
starWarCharacters.remove("Luan")
print(starWarCharacters)

//4. Contains: The same with Array
if starWarCharacters.contains("Duy") {
    print("The list has Duy")
}
else {
    print("The list hasn't Duy")
}
//5. Iterate a set
for starWarCharacter in starWarCharacters {
    print(starWarCharacter)
}

//6. Intersection & union & subtracting & reverse intersection between two set, could apply for two array as well
let set1: Set = [1,2,3,4,5]
let set2: Set = [3,4,5,6,7]
let intersectionResult = set1.intersection(set2).sorted()
let unionResult = set1.union(set2).sorted()
// Check the existing values of set2, if they are also available in set 1, delete it
let subtractingResultSet1 = set1.subtracting(set2).sorted()
// Vice versus
let subtractingResultSet2 = set2.subtracting(set1).sorted()
// symmetricDifference a.k.a reverse intersection
let symmetricDifferenceResult = set1.symmetricDifference(set2).sorted()

//7. Comparison between two Set
let num1:Set = [1,2,3]
let num2:Set = [1,2,3,4,5]
let num3:Set = [7,8,9]
// Check if num1 is superset of num2
num1.isSubset(of: num2)
// Check if num2 is subset of num1
num2.isSuperset(of: num1)
// Check if num1 and num3 is different from each other completely
num1.isDisjoint(with: num3)

