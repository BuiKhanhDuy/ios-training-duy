//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"
// Tuples có nét gì đó hao hao giống closure, nhưng nó giống như 1 cái gói á, thay vì trả về 1 giá trị nó có thể trả về 1 đống giá trị trong cái gói nớ
// Unnamed Tuples
let tipAndTotal = (4.00, 25.19)
print(tipAndTotal.0)
print(tipAndTotal.1)

let tipAndTotal01: (Double, Double) = (2.12, 3.21)

// Assign it
let (theTipTemp, theTotalTemp) = tipAndTotal
print(theTipTemp)
print(theTotalTemp)

// Named Tuples
// Using named tuped can make the code be more meaningful and easier to read as well as maintain

let tipAndTotalNamed = (tipTemp: 12.12, totalTemp: 21.31)
tipAndTotalNamed.tipTemp
tipAndTotalNamed.totalTemp

let tipAndTotalNamed01: (tipTemp01: Double, totalTemp01: Double) = (12.222, 45.55)

// Returning Tuples
func calcTipWithTip(tip: Double) -> (tipTemp:Double, totalTemp:Double) {
    return (tip * tip,tip + tip)
}
let tuplesTemp = calcTipWithTip(tip: 10)
tuplesTemp.tipTemp
