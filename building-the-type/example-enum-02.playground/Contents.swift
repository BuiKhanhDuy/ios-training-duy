//: Playground - noun: a place where people can play

import UIKit

enum AppleDevice {
    case appleWatch(String, UIColor)
    case iPhone(Float, UIColor)
}

var myDevice: AppleDevice = .appleWatch("iPhone 8", UIColor.red)
switch myDevice {
case let .appleWatch(name, color):
    print ("Test \(name) and \(color)")
case let .iPhone(iOSVersion, color):
    print("Version is \(iOSVersion) and \(color)")
}

enum AnimationIcon: Character {
    case ant = "🐜"
    case optopus = "🦑"
    case pig = "🐷"
}
print(AnimationIcon.ant.rawValue)
