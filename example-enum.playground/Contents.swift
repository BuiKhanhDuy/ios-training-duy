//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

protocol CanMakeSound {
    func sound()
}

struct Dog: CanMakeSound {
    func sound() {
        print("Gau gau")
    }
}

class Cat: CanMakeSound {
    func sound() {
        print("Meo meo")
    }
}

enum DogStatus: CanMakeSound {
    case Normal, BeHungry
    func sound() {
        switch self {
        case .Normal:
            print("Normal")
        case .BeHungry:
            print("Hungry")
//        default:
//            print("Default")
        }
    }
}
