//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

let sayHelloClosure = { (_ name: String) -> Void in
    print("Hello \(name)")
}

sayHelloClosure("Duy")

({(_ name: String) -> Void in print ("Hello \(name)")})("Duy02")

func testFunc(name: String, closure:(String) -> Void) {
    print ("Main Func of test Func")
    closure(name)
    }

testFunc(name: "Duy03", closure: {(name) in print ("Hello \(name)")})

var sum: (Int, Int) -> Int =
{
    return $0 + $1
}
sum(10,20)
