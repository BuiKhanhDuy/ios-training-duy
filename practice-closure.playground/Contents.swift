//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

let sayHello = { (_ name: String) in print("Haha \(name)") }
sayHello("Duy")


let checkString = { (_ a:Int, _ b:Int) in return a+b }
checkString(10,12)

({(_ num1:Double, _ num2:Double) in return "Dap an la \(num1) va \(num2)" })(12.2,12.3)
