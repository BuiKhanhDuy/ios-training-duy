//: Playground - noun: a place where people can play

import UIKit

class Person {
    var name: String
    var age: Int
    var address: String
    
    init (_ name: String,_ age: Int,_ address: String)
    {
        self.name = name
        self.age = age
        self.address = address
    }
    // convinience ni nó hơi hướng kiểu là kiểu tạo trước cho thoải mái, cho dễ sử dụng
    convenience init() {
        self.init("",0,"")
    }
    
    convenience init(_ name: String)
    {
        self.init()
        self.name = name
    }
    
}
class User
{
    var id: Int
    var name: String
    
    init(_ id: Int, _ name: String) {
        self.id = id
        self.name = name
    }
    
    convenience init() {
        self.init(1,"No Name")
        self.id = 2
        self.name = "Duy"
    }
}

class Student: Person {
    var schoolName: String
    init (_ schoolName: String) {
        self.schoolName = schoolName
        super.init("Inher",12,"Englang") // Can use a init of super class// Must to use designated
        //super.init() //Error: Must call a designated initializer of the superclass 'Person'
    }
    
    convenience init (age: Int) {
        //super.init()//Convenience initializer for 'Student' must delegate (with 'self.init') rather than chaining to a superclass initializer
        self.init("")
    }
}
let v = Person()
let p = User()
let h = Person("Duy")
