//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

func ternaryOperator()
{
    var temperature = 75
    //var temperature = 90
    var weather = ""
    
    weather = temperature > 80 ? "Hot" : "Not Hot"
    print(weather)
}

func nilCoalesing()
{
    var selectedColor : String?
    selectedColor = "Red"
    print("Selected Color: \(selectedColor)")
    
    let color = selectedColor ?? "blue"
    print("Displayed Color: \(color)")
}

nilCoalesing()
ternaryOperator()
