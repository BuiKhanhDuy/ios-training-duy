//: Playground - noun: a place where people can play

import UIKit

//----Test Optional 01: Optional Binding----
//--
func findCompany (company: String) -> String?
{
    // Assign two examples - hard code
    if (company == "Apple")
    {
        return "AAPL"
    }
    else if (company == "Google")
    {
        return "GOOG"
    }
    return nil
}
// Assign an optional parameter
var tempCompany:String? = findCompany(company: "Facebook")
var selectedCompany = tempCompany ?? "Not any company"
// Declare a string
let text = "Company is: "

print(text+selectedCompany)
//let displayingString = text + tempCompany!
//print(displayingString)

// Optional binding - solution 01
if let actualCompany = tempCompany
{
    let displayingString = text + actualCompany
    print(displayingString)
}
else
{
    print("Nothing here")
}

// Optional binding - solution 02
if tempCompany != nil
{
    let displayingString = text + tempCompany!
    print(displayingString)
}
else
{
   print("Nothing")
}


//Test Optional 02: Optional Chaining


// Create a class with two optional variable
class Tax {
    var code: String?
    var price: Double?
}

func findAndInformTax(_ company: String) -> Tax?
{
    // Create two example values
    if (company == "Apple")
    {
        let apple = Tax()
        apple.code = "APPL"
        apple.price = 123.45
        return apple
    }
    if (company == "Google")
    {
        let google = Tax()
        google.code = "GOOG"
        google.price = 234.45
        return google
    }
    else
    {
    return nil
    }
}
    // Optional Binding
    if let actualTax = findAndInformTax("Apple")
    {
        if let actualCode = actualTax.code
        {
            if let actualPrice = actualTax.price
            {
                print("The code of company is: \(actualCode) with the price is: \(actualPrice*100)")
            }
        }
    }
    // Optional Chaining
    if let actualCode02 = findAndInformTax("Google")?.code
        {
            if let actualPrice02 = findAndInformTax("Google")?.price
            {
                print("The code of company is: \(actualCode02) with the price is: \(actualPrice02*100)")
            }
        }

