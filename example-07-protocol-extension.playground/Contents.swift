//: Playground - noun: a place where people can play

import UIKit

protocol SomeProtocol {
    var mustBeSettable: Int { get set }
    var doesNotNeedToBeSettable: Int { get }
}

protocol Bird {
    var canFly: Bool { get }
    func fly()
}

protocol Developer {
    var programmingLanguage : [String] { get }
    var yearOfExperience : Bool { get }
    var name : String { set get }
}

protocol YearOfExperience
{
    var yearsOfExperience : Int { get set }
}

enum ProgrammingLanguage : String
{
    case Swift = "Swift"
    case ObjC = "ObjC"
    case Java = "Java"
}

struct AndroidDev: Developer
{
    var programmingLanguage: [String] {
        return [ProgrammingLanguage.Java.rawValue]
    }
    
    var yearOfExperience: Bool {
        return false
    }
    
    var name: String
}


struct iOSDev: Developer, YearOfExperience
{
    var programmingLanguage: [String]
    {
        return [ProgrammingLanguage.ObjC.rawValue, ProgrammingLanguage.Swift.rawValue]
    }
    var name: String
    var yearsOfExperience: Int
   // var yearOfExperience: Bool
}

extension Developer where Self: YearOfExperience
{
    var yearOfExperience: Bool {
        return true }
}

let duybk = AndroidDev(name:"Duy")
let duybk01 = iOSDev(name: "Duy01", yearsOfExperience: 12)

