//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

var listOfCars: [String] = ["Car 1", "Car 2", "Car 3", "Car 4", "Car 5"]

//func washCar (_ car: String)
func washCar (_ carTemp: @autoclosure ()->String)
{
    var allConditionsAreReady = false
    allConditionsAreReady = arc4random() % 2 == 0 ? true : false
    if allConditionsAreReady { print("Washing car: \(carTemp())")}
    else { print ("Waiting after 1 min")}
}
while(listOfCars.count > 0)
{
        washCar(listOfCars.removeFirst())
//    washCar(listOfCars.removeFirst())
}

func washCar02 (_ car: String)
//func washCar02 (_ car: @autoclosure ()->String)
{
    var allConditionsAreReady = false
    allConditionsAreReady = arc4random() % 2 == 0 ? true : false
    if allConditionsAreReady { print("Washing car: \(car)")}
    else { print ("Waiting after 1 min")}
}
//while(listOfCars.count > 0)
//{
//    //    washCar({ ()-> String in
//    //       listOfCars.removeFirst() })
//    washCar02(listOfCars.removeFirst())
//}
