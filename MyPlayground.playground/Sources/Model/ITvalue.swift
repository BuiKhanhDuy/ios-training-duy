import Foundation

public class ITvalue: GeneralDB {
    public var celsius: Double?
    public var fahrenheit: Double = 0
    public init(id: Int, name: String, celsius: Double = 0) {
        self.celsius = celsius
        self.fahrenheit = (celsius * 1.8) + 32
        super.init(id: id, name: name)
    }
    
    override public init() {
        super.init()
    }
    
    public init?(jsonObject: [String: AnyObject]) {
        guard let id = jsonObject["id"] as? Int,
              let name = jsonObject["name"] as? String,
              let celsius = jsonObject["celsius"] as? Double else {
                return nil
        }
        self.celsius = celsius
        self.fahrenheit = (celsius * 1.8) + 32
        super.init(id: id, name: name)
    }
    public enum TempList {
        case celsius
        case fahrenheit
    }
    
    public func displayTemp(type: TempList) -> String {
        switch type {
        case .celsius:
            return celsius != nil ? "The celsius is \(self.celsius!)": "Not record the value of Infrared Thermometry yet"
        case .fahrenheit:
            return celsius != nil ? "The fahrenheit is \(self.fahrenheit)": "and also can not calculate fahrenheit"
        }
    }
    
    public func displayInfoAsCelsius() -> String {
        return celsius != nil ? "The celsius is \(self.celsius!)": "Not record the value of Infrared Thermometry yet"
    }
    
    public func displayInfoAsFahrenheit() -> String {
       return celsius != nil ? "The fahrenheit is \(self.fahrenheit)": "and also can not calculate fahrenheit"
    }
}
