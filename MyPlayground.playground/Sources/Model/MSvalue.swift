import Foundation

public class MSvalue : GeneralDB {
    public var msPara: Double?
    public init(id: Int, name: String, msPara: Double) {
        self.msPara = msPara
        super.init(id: id, name: name)
    }
    
    override public init() {
        super.init()
    }
    
    public init?(jsonObject: [String: AnyObject]) {
        guard let id = jsonObject["id"] as? Int,
              let name = jsonObject["name"] as? String,
              let msPara = jsonObject["msPara"] as? Double else {
                return nil
        }
        self.msPara = msPara
        super.init(id: id, name: name)
    }
    
    public func displayInfoOfMS() -> String {
        if self.msPara != nil {
            return "The value of Moisture is \(self.msPara!)"
        } else {
            return "Not record Moisture value yet"
        }
    }
}
