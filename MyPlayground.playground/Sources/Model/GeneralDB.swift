import Foundation

public class GeneralDB {
    public var id: Int = 0
    public var name: String = ""
    public init(id: Int, name: String) {
        self.id = id
        self.name = name
    }
    public init() {
    }
   public func displayInfo() -> String {
        return "ID: \(self.id) and name: \(self.name)"
    }
}
