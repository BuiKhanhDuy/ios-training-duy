import Foundation

public class ITvalueArray {
    var arrayIT: [ITvalue] = []
    public init?(jsonObject: [String: AnyObject]) {
        if let arrayTemp = jsonObject["itData"] as? [[String: AnyObject]] {
            for each in arrayTemp {
                let eachIT = ITvalue(jsonObject: each)!
                arrayIT.append(eachIT)
            }
        } else {
            print("Error while parsing IT Values")
        }
    }
}
