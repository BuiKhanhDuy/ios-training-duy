import Foundation

public class DMvalue: GeneralDB {
    public var dmPara: Double?
    public init(id: Int, name: String, dmPara: Double = 0) {
        self.dmPara = dmPara
        super.init(id: id, name: name)
    }
    
    override public init() {
        super.init()
    }
    
    public init?(jsonObject: [String: AnyObject]) {
        guard let id = jsonObject["id"] as? Int,
              let name = jsonObject["name"] as? String,
              let dmPara = jsonObject["dmPara"] as? Double else {
                return nil
        }
        self.dmPara = dmPara
        super.init(id: id, name: name)
    }
    
    public func displayInfoOfDM() -> String {
        if dmPara != nil {
        return "The value of Digital Mulitimeter is: \(self.dmPara!) (meter)"
        }
        return "Not Record Digital Mulitimeter yet"
    }
}
