import Foundation

public class LDMvalueArray {
    var arrayLDM: [LDMvalue] = []
    public init?(jsonObject: [String: AnyObject]) {
        if let arrayTemp = jsonObject["ldmData"] as? [[String: AnyObject]] {
            for each in arrayTemp {
                let eachLDM = LDMvalue(jsonObject: each)!
                arrayLDM.append(eachLDM)
            }
        } else {
            print("Error while parsing JSON")
        }
    }
}
