import Foundation

public class AFvalue: GeneralDB {
    public var valueOfAF: Double?
    public init(id: Int, name: String, valueOfAF: Double) {
        self.valueOfAF = valueOfAF
        super.init(id: id, name: name)
    }
    
    override public init() {
        super.init()
    }
    
    public init?(jsonObject: [String: AnyObject]) {
        guard let id = jsonObject["id"] as? Int,
              let name = jsonObject["name"] as? String,
              let valueOfAF = jsonObject["valueOfAF"] as? Double else {
                return nil
        }
        self.valueOfAF = valueOfAF
        super.init(id: id, name: name)
    }
    
    public func displayInfoAsDegrees() -> String {
        if valueOfAF != nil {
            return "The value of Angle Finder is: " + Common.roundUpDoubleValue(number: self.valueOfAF!) + " degrees"
        }
            return "Not record Angle Finder value yet"
    }
    public func displayInfoAsRadian() -> String {
        if valueOfAF != nil {
            return "the radian value is: " + Common.roundUpDoubleValue(number: (self.valueOfAF!/180)) + " radian"
        }
            return ""
        }
    }
