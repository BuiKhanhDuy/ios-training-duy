import Foundation

public class DMvalueArray {
    var arrayDM: [DMvalue] = []
    public init?(jsonObject: [String: AnyObject]) {
        if let arrayTemp = jsonObject["dmData"] as? [[String: AnyObject]] {
            for each in arrayTemp {
                let eachDMData = DMvalue(jsonObject: each)!
                arrayDM.append(eachDMData)
            }
        } else {
            print("Error while parsing DM Value")
        }
    }
}
