import Foundation

public class AFvalueArray {
    var arrayAF: [AFvalue] = []
    public init?(jsonObject: [String: AnyObject]) {
        if let arrayTemp = jsonObject["afData"] as? [[String: AnyObject]] {
            for each in arrayTemp {
                let eachAF = AFvalue(jsonObject: each)!
                self.arrayAF.append(eachAF)
            }
        } else {
            print("Error while parsing AF from JSON")
        }
    }
}
