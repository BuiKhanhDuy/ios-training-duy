import Foundation

public class Project: GeneralDB {
    public var valueOfAF: AFvalue?
    public var valueOfDM: DMvalue?
    public var valueOfIT: ITvalue?
    public var valueOfLDM: LDMvalue?
    public var valueOfMS: MSvalue?
    public init(id: Int, name: String, valueOfAF: AFvalue = AFvalue() , valueOfDM: DMvalue = DMvalue(), valueOfIT: ITvalue = ITvalue(), valueOfLDM: LDMvalue = LDMvalue(), valueOfMS: MSvalue = MSvalue()) {
        self.valueOfAF = valueOfAF
        self.valueOfDM = valueOfDM
        self.valueOfIT = valueOfIT
        self.valueOfLDM = valueOfLDM
        self.valueOfMS = valueOfMS
        super.init(id: id, name: name)
    }
    override public init() {
        super.init()
    }
    public init?(jsonObject: [String: AnyObject]) {
        guard let id = jsonObject["id"] as? Int,
              let name = jsonObject["name"] as? String else {
                return nil
        }
        if let valueOfAFraw = jsonObject["valueOfAF"] as? [String: AnyObject] {
            if let tempAF = AFvalue(jsonObject: valueOfAFraw) {
                self.valueOfAF = tempAF
            }
        }
        if let valueOfDMraw = jsonObject["valueOfDM"] as? [String: AnyObject] {
            if let tempDM = DMvalue(jsonObject: valueOfDMraw) {
                self.valueOfDM = tempDM
            }
        }
        if let valueOfITraw = jsonObject["valueOfIT"] as? [String: AnyObject] {
            if let tempIT = ITvalue(jsonObject: valueOfITraw) {
                self.valueOfIT = tempIT
            }
        }
        if let valueOfLDMraw = jsonObject["valueOfLDM"] as? [String: AnyObject] {
            if let tempLDM = LDMvalue(jsonObject: valueOfLDMraw) {
                self.valueOfLDM = tempLDM
            }
        }
        if let valueOfMSraw = jsonObject["valueOfMS"] as? [String: AnyObject] {
            if let tempMS = MSvalue(jsonObject: valueOfMSraw) {
                self.valueOfMS = tempMS
            }
        }
        super.init(id: id, name: name)
    }
    
    public func displayAllInformation() {
        let infoCritia = "Your project has id: \(id) and name: \(name)"
        let infoAF = self.valueOfAF!.displayInfoAsDegrees() + " and " + self.valueOfAF!.displayInfoAsRadian()
        let infoDM = self.valueOfDM!.displayInfoOfDM()
        let infoIT = self.valueOfIT!.displayInfoAsCelsius() + " and " + self.valueOfIT!.displayInfoAsFahrenheit()
        let infoLDM = self.valueOfLDM!.displayInfoAsArea() + " and " + self.valueOfLDM!.displayInfoAsPerimeter() + " and " + self.valueOfLDM!.displayInfoAsVolume()
        let infoMS = self.valueOfMS!.displayInfoOfMS()
        print(infoCritia + "\n" + infoAF + "\n" + infoDM + "\n" + infoMS + "\n" + infoLDM + "\n" + infoIT)
    }
}
