import Foundation

public class LDMvalue: GeneralDB {
    var height: Double?
    var width: Double?
    var length: Double?
    public init(id: Int, name: String, height: Double, width: Double, length: Double) {
        self.height = height
        self.width = width
        self.length = length
        super.init(id: id, name: name)
    }
    
    override public init() {
        super.init()
    }
    
    public init?(jsonObject: [String: AnyObject])
    {
        guard let id = jsonObject["id"] as? Int,
              let name = jsonObject["name"] as? String,
              let height = jsonObject["height"] as? Double,
              let width = jsonObject["width"] as? Double,
              let length = jsonObject["length"] as? Double else {
                return nil
        }
        self.height = height
        self.width = width
        self.length = length
        super.init(id: id, name: name)
    }
    
    public func displayInfoAsArea() -> String {
        if (self.length != nil) && (self.width != nil) {
            return "The area is: \(self.length! * self.width!)"
        } else {
            return "Not record Lazer Distance Measure Parameter yet"
        }
    }
    
    public func displayInfoAsPerimeter() -> String {
        if (self.length != nil) && (self.width != nil) {
            return "The perimeter is: \((self.length! + self.width!) * 2)"
        } else {
            return ""
        }
    }
    
    public func displayInfoAsVolume() -> String {
        if (self.length != nil) && (self.width != nil) && (self.height != nil) {
            return "The volume is: \(self.height! * self.width! * self.length!)"
        } else {
            return ". And not enough value to calculate the volume"
        }
    }
}
