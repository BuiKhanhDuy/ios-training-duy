import Foundation

public class MSvalueArray {
    var arrayMS:[MSvalue] = []
    public init?(jsonObject: [String: AnyObject]) {
        if let arrayTemp = jsonObject["msData"] as? [[String: AnyObject]] {
            for each in arrayTemp {
                let eachMS = MSvalue(jsonObject: each)!
                arrayMS.append(eachMS)
            }
        } else {
            print("Error while parsing JSON")
        }
    }
}
