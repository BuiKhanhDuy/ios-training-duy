import Foundation

public class ProjectArray {
    var arrayPj : [Project] = []
    public init? (jsonObject: [String: AnyObject]) {
        if let project = jsonObject["project"] as? [[String: AnyObject]] {
            for each in project {
                if let projectTemp = Project(jsonObject: each) {
                    self.arrayPj.append(projectTemp)
                }
            }
        }
    }
}
