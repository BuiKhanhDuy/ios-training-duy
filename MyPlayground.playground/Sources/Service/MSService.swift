import Foundation

public class MSService {
    public static var listMSValues: [MSvalue] = []
    public static func getValueByID (id: Int) -> MSvalue? {
        var objectTemp: MSvalue?
        for each in listMSValues {
            if each.id == id {
                objectTemp = each
                break
            }
        }
        return objectTemp
    }
    
    public static func addNewObjectIntoList(object: MSvalue) {
        listMSValues.append(object)
    }
    
    public static func returnListMS() -> [MSvalue] {
        return listMSValues
    }
    
    public static func parseJSON() -> [MSvalue] {
        var msArrays: [MSvalue] = []
        if let msArrayTemp = MSvalueArray(jsonObject: Common.getJSONDataRaw(forResource: "fakeData", ofType: "json")) {
            msArrays = msArrayTemp.arrayMS
        } else {
            print("parseJSON is wrong")
        }
        return msArrays
    }
    
    public static func readDataJSONDocDir() -> [MSvalue] {
        var msArrays: [MSvalue] = []
        if let documentDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            do {
                let fileUrl = documentDirectoryUrl.appendingPathComponent("fakeData.json")
                let data =  try Data(contentsOf: fileUrl)
                let jsonResultRaw: [String: AnyObject] = (try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject])!
                if let msArrayTemp = MSvalueArray(jsonObject: jsonResultRaw) {
                    msArrays = msArrayTemp.arrayMS
                }
            } catch {
                print("Can not load data")
            }
        }
        return msArrays
    }
}
