import Foundation

public class DMService {
    public static var listDMValues: [DMvalue] = []
    public static func getValueByID(id: Int) -> DMvalue? {
        var objectTemp: DMvalue?
        for each in listDMValues {
            if each.id == id {
                objectTemp = each
                break
            }
        }
        return objectTemp
    }
    
    public static func addNewObjectIntoList(object: DMvalue) {
        listDMValues.append(object)
    }
    
    public static func returnListDM() -> [DMvalue] {
        return listDMValues
    }
    
    public static func parseJSON() -> [DMvalue] {
        var dmArrays: [DMvalue] = []
        if let dmArrayTemp = DMvalueArray(jsonObject: Common.getJSONDataRaw(forResource: "fakeData", ofType: "json")) {
            dmArrays = dmArrayTemp.arrayDM
        } else {
            print("parseJSON is wrong")
        }
        return dmArrays
    }
    
    public static func readDataJSONDocDir() -> [DMvalue] {
        var dmArrays: [DMvalue] = []
        if let documentDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            do {
                let fileUrl = documentDirectoryUrl.appendingPathComponent("fakeData.json")
                let data =  try Data(contentsOf: fileUrl)
                let jsonResultRaw: [String: AnyObject] = (try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject])!
                if let dmArrayTemp = DMvalueArray(jsonObject: jsonResultRaw) {
                    dmArrays = dmArrayTemp.arrayDM
                }
            } catch {
                print("Can not load data")
            }
        }
        return dmArrays
    }
}
