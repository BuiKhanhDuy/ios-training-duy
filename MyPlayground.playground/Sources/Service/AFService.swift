import Foundation

public class AFService {
    public static var listAFValues: [AFvalue] = []
    public static func getValueByID(id: Int) -> AFvalue? {
        var objectTemp: AFvalue?
        for each in listAFValues {
            if each.id == id {
                objectTemp = each
                break
            }
        }
        return objectTemp
    }
    
    public static func addNewObjectIntoList(object: AFvalue) {
        listAFValues.append(object)
    }
    
    public static func returnListAF() -> [AFvalue] {
        return listAFValues
    }
    
//    public static func parseJSON() -> [AFvalue] {
//    var afArrays: [AFvalue] = []
//    if let path = Bundle.main.path(forResource: "fakeData", ofType: "json") {
//        do {
//            let data =  try Data(contentsOf: URL(fileURLWithPath: path))
//            let jsonResultRaw: [String: AnyObject] = (try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject])!
//            if let afArrayTemp = AFvalueArray(jsonObject: jsonResultRaw) {
//                afArrays = afArrayTemp.arrayAF
//            }
//        } catch {
//            print("Can not load data")
//        }
//    } else {
//        print("Path is wrong")
//    }
//        return afArrays
//    }
        public static func parseJSON() -> [AFvalue] {
        var afArrays: [AFvalue] = []
            if let afArrayTemp = AFvalueArray(jsonObject: Common.getJSONDataRaw(forResource: "fakeData", ofType: "json")) {
                    afArrays = afArrayTemp.arrayAF
                } else {
            print("parseJSON is wrong")
        }
            return afArrays
        }
    public static func readDataJSONDocDir() -> [AFvalue] {
        var afArrays: [AFvalue] = []
        if let documentDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            do {
                let fileUrl = documentDirectoryUrl.appendingPathComponent("fakeData.json")
                let data =  try Data(contentsOf: fileUrl)
                let jsonResultRaw: [String: AnyObject] = (try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject])!
                if let afArrayTemp = AFvalueArray(jsonObject: jsonResultRaw) {
                    afArrays = afArrayTemp.arrayAF
                }
            } catch {
                print("Can not load data")
            }
        }
        return afArrays
    }
}
