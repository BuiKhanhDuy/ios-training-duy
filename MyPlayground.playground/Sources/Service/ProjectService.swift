import Foundation

public class ProjectService {
    public static var listProjectValues: [Project] = []
    public static func getValueByID (id: Int) -> Project? {
        var objectTemp: Project?
        for each in listProjectValues {
            if each.id == id {
                objectTemp = each
                break
            }
        }
        return objectTemp
    }
    
    public static func addNewObjectIntoList(object: Project) {
        listProjectValues.append(object)
    }
    
    public static func returnListProject() -> [Project] {
            return listProjectValues
    }
    
    public static func parseJSON() -> [Project] {
        var pjArrays: [Project] = []
        if let pjArrayTemp = ProjectArray(jsonObject: Common.getJSONDataRaw(forResource: "projectData", ofType: "json")) {
            pjArrays = pjArrayTemp.arrayPj
        } else {
            print("parseJSON is wrong")
        }
        return pjArrays
    }
}
