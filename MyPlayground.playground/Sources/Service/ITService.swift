import Foundation

public class ITService {
    public static var listITValues: [ITvalue] = []
    public static func getValueByID (id: Int) -> ITvalue? {
        var objectTemp: ITvalue?
        for each in listITValues {
            if each.id == id {
                objectTemp = each
                break
            }
        }
        return objectTemp
    }
    
    public static func addNewObjectIntoList(object: ITvalue) {
        listITValues.append(object)
    }
    
    public static func returnListIT() -> [ITvalue] {
        return listITValues
    }
    
    public static func parseJSON() -> [ITvalue] {
        var itArrays: [ITvalue] = []
        if let itArrayTemp = ITvalueArray(jsonObject: Common.getJSONDataRaw(forResource: "fakeData", ofType: "json")) {
            itArrays = itArrayTemp.arrayIT
        } else {
            print("parseJSON is wrong")
        }
        return itArrays
    }
    
    public static func readDataJSONDocDir() -> [ITvalue] {
        var itArrays: [ITvalue] = []
        if let documentDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            do {
                let fileUrl = documentDirectoryUrl.appendingPathComponent("fakeData.json")
                let data =  try Data(contentsOf: fileUrl)
                let jsonResultRaw: [String: AnyObject] = (try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject])!
                if let itArrayTemp = ITvalueArray(jsonObject: jsonResultRaw) {
                    itArrays = itArrayTemp.arrayIT
                }
            } catch {
                print("Can not load data")
            }
        }
        return itArrays
    }
}
