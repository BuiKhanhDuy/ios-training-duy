import Foundation

public class HandleEvent {
    static var flag1: Int = 0
    static var yourProject01 = Project()
    public static func displayThePractice(flag: Int = 0) {
        print(Common.firstDescription)
        if flag == 0 {
            let selectStep01: Int = Int(arc4random_uniform(3)) + 1
            switch selectStep01 {
            case 1:
                print("You selected 1. Add new project")
                HandleEvent.addNewProject()
                HandleEvent.showResults()
            case 2:
                print("You selected 2. Display all project")
                if (ProjectService.returnListProject().count == 0) {
                    print("There is no project here. Please add new one.")
                } else {
                    print("Below is all your projects")
                    for eachProject in ProjectService.returnListProject() {
                        eachProject.displayAllInformation()
                        print("====")
                }
            }
            case 3:
                print("You selected 3. View details of tool")
                viewDetailedOfProject()
            default:
                break
            }
        } else if flag == 1 {
        }
    }
    static func showResults() {
        print("You have just add the following project: ")
        let valueOfAFtemp = Common.projectTemp.valueOfAF ?? AFvalue()
        let valueOfDMtemp = Common.projectTemp.valueOfDM ?? DMvalue()
        let valueOfITtemp = Common.projectTemp.valueOfIT ?? ITvalue()
        let valueOfLDMtemp = Common.projectTemp.valueOfLDM ?? LDMvalue()
        let valueOfMStemp = Common.projectTemp.valueOfMS ?? MSvalue()
        yourProject01 = Project(id: 4, name: "New Project", valueOfAF: valueOfAFtemp, valueOfDM: valueOfDMtemp, valueOfIT: valueOfITtemp, valueOfLDM: valueOfLDMtemp, valueOfMS: valueOfMStemp)
        yourProject01.displayAllInformation()
        ProjectService.addNewObjectIntoList(object: yourProject01)
    }
    
    static func addNewProject() {
        print("Please input the id and name of your project: ")
        selectTools()
    }
    
    static func selectTools() {
        print(Common.descriptionChooseTools)
        let selectstep02:Int = Int(arc4random_uniform(5)) + 1
        switch selectstep02 {
        case 1:
            HandleEvent.handleLDM()
        case 2:
            HandleEvent.handleAF()
        case 3:
            HandleEvent.handleIT()
        case 4:
            HandleEvent.handleDM()
        case 5:
            HandleEvent.handleMS()
        default:
            break
        }
    }
    
    static func handleLDM() {
        print("Welcome to LDM tool. " + Common.descriptionCreateTools)
        let selectLDM: Int = Int(arc4random_uniform(2)) + 1
        var ldm01 = LDMvalue()
        switch selectLDM {
        case 1:
            print("Input new one")
            ldm01 = LDMvalue(id: 4, name: "DataLDM04", height: 13.14, width: 10.10, length: 5.6)
            LDMService.addNewObjectIntoList(object: ldm01)
            Common.projectTemp.valueOfLDM = ldm01
            print("You added a new LDM value which has \(ldm01.displayInfo())")
            print("Selected LDM successfully")
            HandleEvent.QuitMessage()
        case 2:
            for each in LDMService.returnListLDM() {
                print("Info: \(each.displayInfo()). Length is: \(each.length!), width is \(each.width!), height is \(each.height!). Area is \(each.displayInfoAsArea()), perimeter is \(each.displayInfoAsPerimeter()), volume is \(each.displayInfoAsVolume())")
            }
            print("Select an id of your favorite LDM from the array")
            let idPress: Int = 2
            ldm01 = LDMService.getValueByID(id: idPress)!
            Common.projectTemp.valueOfLDM = ldm01
            print("You selected an existing LDM value which has \(ldm01.displayInfo())")
            print("Selected LDM successfully")
            HandleEvent.QuitMessage()
        default:
            break
        }
    }

    static func handleAF() {
        print("Welcome to AF tool. " + Common.descriptionCreateTools)
        let selectAF: Int = Int(arc4random_uniform(2)) + 1
        var af01 = AFvalue()
        switch selectAF {
        case 1:
            print("Input new one")
            af01 = AFvalue(id: 4, name: "DataOfAF04", valueOfAF: 12.12)
            Common.projectTemp.valueOfAF = af01
            AFService.addNewObjectIntoList(object: af01)
            print("You added a new AF value which has \(af01.displayInfo())")
            print("Selected AF successfully")
            HandleEvent.QuitMessage()
        case 2:
            for each in AFService.returnListAF() {
                print("\(each.displayInfo()) + \(each.displayInfoAsRadian()) + \(each.displayInfoAsDegrees())")
            }
            print("Select an id of your favorite AF from the array")
            let idPress: Int = 1
            af01 = AFService.getValueByID(id: idPress)!
            Common.projectTemp.valueOfAF = af01
            print("You selected an existing AF value which has \(af01.displayInfo())")
            print("Selected AF successfully")
            HandleEvent.QuitMessage()
        default:
            break
        }
    }
    
    static func handleIT() {
        print("Welcome to IT tool. " + Common.descriptionCreateTools)
        let selectIT: Int = Int(arc4random_uniform(2)) + 1
        var it01 = ITvalue()
        switch selectIT {
        case 1:
            print("Input new one")
            it01 = ITvalue(id: 4, name: "DataOfIT04", celsius: 37.5)
            Common.projectTemp.valueOfIT = it01
            ITService.addNewObjectIntoList(object: it01)
            print("You added a new IT value which has \(it01.displayInfo())")
            print("Selected IT successfully")
            HandleEvent.QuitMessage()
        case 2:
            for each in ITService.returnListIT()
            {
                print("\(each.displayInfo()) + \(each.displayInfoAsCelsius()) + \(each.displayInfoAsFahrenheit())")
            }
            print("Select an id of your favorite IT from the array")
            let idPress: Int = 3
            it01 = ITService.getValueByID(id: idPress)!
            Common.projectTemp.valueOfIT = it01
            print("You selected an existing IT value which has \(it01.displayInfo())")
            print("Selected IT successfully")
            HandleEvent.QuitMessage()
        default:
            break
        }
    }
    
    static func handleDM() {
        print("Welcome to DM tool. " + Common.descriptionCreateTools)
        let selectDM: Int = Int(arc4random_uniform(2)) + 1
        var dm01 = DMvalue()
        switch selectDM {
        case 1:
            print("Input new one")
            dm01 = DMvalue(id: 4, name: "DataOfDM04", dmPara: 137.5)
            Common.projectTemp.valueOfDM = dm01
            DMService.addNewObjectIntoList(object: dm01)
            print("You added a new DM value which has \(dm01.displayInfo())")
            print("Selected DM successfully")
            HandleEvent.QuitMessage()
        case 2:
            for each in DMService.returnListDM() {
                print("\(each.displayInfo()) and \(each.displayInfoOfDM()) )")
            }
            print("Select an id of your favorite DM from the array")
            let idPress: Int = 3
            dm01 = DMService.getValueByID(id: idPress)!
            Common.projectTemp.valueOfDM = dm01
            print("You selected an existing DM value which has \(dm01.displayInfo())")
            print("Selected DM successfully")
            HandleEvent.QuitMessage()
        default:
            break
        }
    }
    
    static func handleMS() {
        print("Welcome to MS tool. " + Common.descriptionCreateTools)
        let selectMS: Int = Int(arc4random_uniform(2)) + 1
        var ms01 = MSvalue()
        switch selectMS {
        case 1:
            print("Input new one")
            ms01 = MSvalue(id: 4, name: "DataOfMS04", msPara: 77.5)
            Common.projectTemp.valueOfMS = ms01
            MSService.addNewObjectIntoList(object: ms01)
            print("You added a new MS value which has \(ms01.displayInfo())")
            print("Selected MS successfully")
            HandleEvent.QuitMessage()
        case 2:
            for each in MSService.returnListMS() {
                print("\(each.displayInfo()) and \(each.displayInfoOfMS()) )")
            }
            print("Select an id of your favorite MS from the array")
            let idPress: Int = 2
            ms01 = MSService.getValueByID(id: idPress)!
            Common.projectTemp.valueOfMS = ms01
            print("You added an existing MS value which has \(ms01.displayInfo())")
            print("Selected MS successfully")
            HandleEvent.QuitMessage()
        default:
            break
        }
    }
    
    static func viewDetailedOfProject() {
        print("Please input id of your project")
        let selectedProject: Int = Int(arc4random_uniform(3)) + 1
        print("You selected the project has id: \(selectedProject)")
        let project: Project = ProjectService.getValueByID(id: selectedProject)!
        viewSelectedTool(project: project)
        backMessage(project: project)
    }
    
    static func viewSelectedTool(project: Project) {
        print(Common.desDetailedTools)
        let selectedTool: Int = Int(arc4random_uniform(6)) + 1
        print("You selected \(selectedTool)")
        switch selectedTool {
        case 1:
            project.displayAllInformation()
        case 2:
            print("The height is = \(project.valueOfLDM!.height!). The width is = \(project.valueOfLDM!.width!). The length is = \(project.valueOfLDM!.length!)" + project.valueOfLDM!.displayInfoAsArea() + " and " + project.valueOfLDM!.displayInfoAsPerimeter() + " and " + project.valueOfLDM!.displayInfoAsVolume())
        case 3:
            print(project.valueOfAF!.displayInfoAsDegrees() + ". " + project.valueOfAF!.displayInfoAsRadian())
        case 4:
            print(project.valueOfIT!.displayInfoAsCelsius() + ". " + project.valueOfIT!.displayInfoAsFahrenheit())
        case 5:
            print(project.valueOfDM!.displayInfoOfDM())
        case 6:
            print(project.valueOfMS!.displayInfoOfMS())
        default:
            break
        }
    }
    
    static func backMessage(project: Project) {
        let ask: Int = Int(arc4random_uniform(2)) + 1
        print(Common.askYesNoDetailed)
        if ask == 2 {
            print("You have already selected to quit. Processing to back to Home...")
            HandleEvent.displayThePractice(flag: 1)
        } else {
            print("You have already selected to continue. Now continue...")
            HandleEvent.viewSelectedTool(project: project)
        }
    }
    
    static func QuitMessage() {
        let ask: Int = Int(arc4random_uniform(2)) + 1
        print(Common.askYesNo)
        if ask == 2 {
            print("You have already selected to quit. Processing to back to Home...")
            HandleEvent.displayThePractice(flag: 1)
        } else {
            print("You have already selected to continue. Now continue...")
            HandleEvent.selectTools()
        }
    }
}
