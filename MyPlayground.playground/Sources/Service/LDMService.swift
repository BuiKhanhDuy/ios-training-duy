import Foundation

public class LDMService {
    public static var listLDMValues: [LDMvalue] = []
    public static func getValueByID (id: Int) -> LDMvalue? {
        var objectTemp: LDMvalue?
        for each in listLDMValues {
            if each.id == id {
                objectTemp = each
                break
            }
        }
        return objectTemp
    }
    
    public static func addNewObjectIntoList(object: LDMvalue) {
        listLDMValues.append(object)
    }
    
    public static func returnListLDM() -> [LDMvalue] {
        return listLDMValues
    }
    
    public static func parseJSON() -> [LDMvalue] {
        var ldmArrays: [LDMvalue] = []
        if let ldmArrayTemp = LDMvalueArray(jsonObject: Common.getJSONDataRaw(forResource: "fakeData", ofType: "json")) {
            ldmArrays = ldmArrayTemp.arrayLDM
        } else {
            print("parseJSON is wrong")
        }
        return ldmArrays
    }
    
    public static func readDataJSONDocDir() -> [LDMvalue] {
        var ldmArrays: [LDMvalue] = []
        if let documentDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            do {
                let fileUrl = documentDirectoryUrl.appendingPathComponent("fakeData.json")
                let data =  try Data(contentsOf: fileUrl)
                let jsonResultRaw: [String: AnyObject] = (try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject])!
                if let ldmArrayTemp = LDMvalueArray(jsonObject: jsonResultRaw) {
                    ldmArrays = ldmArrayTemp.arrayLDM
                }
            } catch {
                print("Can not load data")
            }
        }
        return ldmArrays
    }
}
