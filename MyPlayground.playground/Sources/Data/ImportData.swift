import Foundation

public class ImportData {
    public static func loadInitializedData() {
        for each in MSService.parseJSON() {
            MSService.addNewObjectIntoList(object: each)
        }
        
        for each in AFService.parseJSON() {
            AFService.addNewObjectIntoList(object: each)
        }
        
        for each in DMService.parseJSON() {
            DMService.addNewObjectIntoList(object: each)
        }
        
        for each in LDMService.parseJSON() {
            LDMService.addNewObjectIntoList(object: each)
        }
        
        for each in ITService.parseJSON() {
            ITService.addNewObjectIntoList(object: each)
        }
        
        for each in ProjectService.parseJSON() {
            ProjectService.addNewObjectIntoList(object: each)
        }
    }
    
    public static func updatingData(){
        guard let documentDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else {
            return
        }
        let fileUrl = documentDirectoryUrl.appendingPathComponent("fakeData.json")
        do {
            let data = try JSONSerialization.data(withJSONObject: subscriptForWritingData(), options: JSONSerialization.WritingOptions.prettyPrinted)
            try data.write(to: fileUrl, options: [])
        } catch {
            print(error)
        }
    }
    
    public static func reLoadData() {
        updatingData()
        for each in MSService.readDataJSONDocDir() {
            MSService.addNewObjectIntoList(object: each)
        }
        
        for each in AFService.readDataJSONDocDir() {
            AFService.addNewObjectIntoList(object: each)
        }
        
        for each in DMService.readDataJSONDocDir() {
            DMService.addNewObjectIntoList(object: each)
        }
        
        for each in LDMService.readDataJSONDocDir() {
            LDMService.addNewObjectIntoList(object: each)
        }
        
        for each in ITService.readDataJSONDocDir() {
            ITService.addNewObjectIntoList(object: each)
        }
    }

    public static func subscriptForWritingData() -> [String: AnyObject] {
        var thedictAll: [String: AnyObject] = [:]
        thedictAll["name"] = "AllDatabase" as AnyObject
        
        // xử lý thèng AF
        var arrayAF: [AnyObject] = []
        for eachAF in AFService.parseJSON() {
            var eachObject: [String: AnyObject] = [:]
            eachObject["id"] = eachAF.id as AnyObject
            eachObject["name"] = eachAF.name as AnyObject
            eachObject["valueOfAF"] = eachAF.valueOfAF as AnyObject
            arrayAF.append(eachObject as AnyObject)
        }
        thedictAll["afData"] = arrayAF as AnyObject
        
        // xu ly tiep cai thang` dm
        var arrayDM: [AnyObject] = []
        for eachDM in DMService.parseJSON() {
            var eachObject: [String: AnyObject] = [:]
            eachObject["id"] = eachDM.id as AnyObject
            eachObject["name"] = eachDM.name as AnyObject
            eachObject["dmPara"] = eachDM.dmPara as AnyObject
            arrayDM.append(eachObject as AnyObject)
        }
        thedictAll["dmData"] = arrayDM as AnyObject
        
        
        //xu ly tiep cai thang ldm
        var arrayLDM: [AnyObject] = []
        for eachLDM in LDMService.parseJSON() {
            var eachObject: [String: AnyObject] = [:]
            eachObject["id"] = eachLDM.id as AnyObject
            eachObject["name"] = eachLDM.name as AnyObject
            eachObject["height"] = eachLDM.height! as AnyObject
            eachObject["length"] = eachLDM.length! as AnyObject
            eachObject["width"] = eachLDM.width! as AnyObject
            arrayLDM.append(eachObject as AnyObject)
        }
        thedictAll["ldmData"] = arrayLDM as AnyObject
        
        // xu ly tiep cai thang itAnyObject
        var arrayIT: [AnyObject] = []
        for eachIT in ITService.parseJSON() {
            var eachObject: [String: AnyObject] = [:]
            eachObject["id"] = eachIT.id as AnyObject
            eachObject["name"] = eachIT.name as AnyObject
            eachObject["celsius"] = eachIT.celsius as AnyObject
            arrayIT.append(eachObject as AnyObject)
        }
        thedictAll["itData"] = arrayIT as AnyObject
        
        // xu ly tiep cai thang msAnyObject
        var arrayMS: [AnyObject] = []
        for eachMS in MSService.parseJSON() {
            var eachObject: [String: AnyObject] = [:]
            eachObject["id"] = eachMS.id as AnyObject
            eachObject["name"] = eachMS.name as AnyObject
            eachObject["msPara"] = eachMS.msPara as AnyObject
            arrayMS.append(eachObject as AnyObject)
        }
        thedictAll["msData"] = arrayMS as AnyObject
        return thedictAll
    }
    
}
