import Foundation

public class Common {
    public static var descriptionChooseTools = """
                ========Choose your tools========
                 1. Lazer Distance Measure
                 2. Angle Finder
                 3. Infrared Thermometry
                 4. Digital Multimeter
                 5. Moisture
              """
    public static var askYesNo = "Do you want to continue to input others tool? (Press 1: Yes to continue, Press 2: Quit to Home)"
    public static var desDetailedTools = """
                ========Choose your tools========
                 1. View All Information
                 2. View Lazer Distance Measure
                 3. View Angle Finder
                 4. View Infrared Thermometry
                 5. View Digital Multimeter
                 6. View Moisture
              """
    public static var askYesNoDetailed = "Do you want to continue to view others tool? (Press 1: Yes to Continue, Press 2: Quit to Home)"
    public static var firstDescription = """
                ========Welcome to ToolSmart========
                    1. Add new Project
                    2. Display all Project
                    3. View details of tool
              """
    public static var descriptionCreateTools = "Press 1 to add the new tool. Press 2 to get from existing tools"
    public static var projectTemp: Project = Project()
    public static func roundUpDoubleValue(number: Double) -> String {
        return String(format: "%.3f", arguments: [number])
    }
    public static func getJSONDataRaw(forResource: String, ofType: String) -> [String: AnyObject] {
        var jsonRaw: [String: AnyObject] = [:]
        if let path = Bundle.main.path(forResource: forResource, ofType: ofType) {
            do {
                let data =  try Data(contentsOf: URL(fileURLWithPath: path))
                jsonRaw = (try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject])!
            } catch {
                print("Can not load data")
            }
        } else {
            print("Path is wrong")
            return jsonRaw
        }
        return jsonRaw
    }
}
