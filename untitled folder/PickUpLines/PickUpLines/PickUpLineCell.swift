//
//  PickUpLineCell.swift
//  PickUpLines
//
//  Created by duybui on 11/24/17.
//  Copyright © 2017 duybui. All rights reserved.
//

import UIKit

class PickUpLineCell: UITableViewCell {

    @IBOutlet weak var lineLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    
    func configure(with pickUpLine: Any) {
        lineLabel.text = ""
        scoreLabel.text = ""
        emailLabel.text = ""
    }
}
