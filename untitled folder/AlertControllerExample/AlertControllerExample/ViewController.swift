//
//  ViewController.swift
//  AlertControllerExample
//
//  Created by duybui on 11/24/17.
//  Copyright © 2017 duybui. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var alertController: UIAlertController?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func btnShowAlert(_ sender: Any) {
        alertController = UIAlertController(title: "Alert", message: "Are you sure", preferredStyle: .actionSheet)
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive) { (action) in
            print("Press cancel")
        }
        let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
            print("Press OK")
        }
        alertController?.addAction(okAction)
        alertController?.addAction(cancelAction)
        self.present(alertController!, animated: true) {
            print("presented")
        }
    }
    
    
    @IBAction func btnShowAlert2(_ sender: Any) {
        alertController = UIAlertController(title: "Take Image", message: "Choose image source", preferredStyle: .actionSheet)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

