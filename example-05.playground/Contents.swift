//: Playground - noun: a place where people can play
// Nested type - irhen init - Any Object - as
import UIKit

class Student
{
    var _id:Int
    var _name:String
    init(id: Int, name:String) {
        _id = id
        _name = name
    }
}

class AcademicStudent : Student
{
    enum Degree
    {
        case BSc, MSc, MBA, Phd
    }
    var _degree : Degree
    init (id:Int, name:String, degree:Degree)
    {
        _degree = degree
        super.init(id: id, name: name)
    }
}

let vec: [Any] = [
    0.0,
    0,
    AcademicStudent(id: 123, name: "Duy Bui", degree: AcademicStudent.Degree.BSc),
    AcademicStudent(id: 234, name: "Asnet", degree:
    AcademicStudent.Degree.MBA),
    12.5,
    true,
    "world"
]

var number:Int = 0

for ob in vec
{
    if let academic = ob as? AcademicStudent
    {
        if academic._degree == AcademicStudent.Degree.MBA
        {
            number=number+1
        }
    }
}
print(number)
