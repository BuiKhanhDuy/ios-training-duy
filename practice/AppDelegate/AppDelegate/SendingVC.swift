//
//  SendingVC.swift
//  AppDelegate
//
//  Created by duybui on 11/2/17.
//  Copyright © 2017 duybui. All rights reserved.
//

import UIKit
protocol DataEntryDelegate
{
   func displayData(data: String)
}
class SendingVC: UIViewController {
    var delegate: DataEntryDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBOutlet weak var textInSendingVC: UITextField!
    
    @IBAction func sendingButtonWasPressed(_ sender: UIButton) {
        if let delegate = delegate
        {
            if textInSendingVC.text != nil
            {
                let data = textInSendingVC.text
                delegate.displayData(data: data!)
                dismiss(animated: true, completion: nil)
            }
        }
    }
        
}

