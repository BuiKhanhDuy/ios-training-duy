//
//  ReceivingVC.swift
//  AppDelegate
//
//  Created by duybui on 11/2/17.
//  Copyright © 2017 duybui. All rights reserved.
//

import UIKit

class ReceivingVC: UIViewController, DataEntryDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    @IBOutlet weak var textInReceivingVC: UITextView!
    func displayData(data: String) {
        textInReceivingVC.text = data
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showSendingVC"
        {
        let sendingVC: SendingVC = segue.destination as! SendingVC
        sendingVC.delegate = self
        }
    }
}

