//
//  AddPetTableViewController.swift
//  AppDuy02
//
//  Created by duybui on 11/2/17.
//  Copyright © 2017 duybui. All rights reserved.
//

import UIKit

class AddPetTableViewController: UITableViewController {
    @IBOutlet weak var textName: UITextField!
    @IBOutlet weak var textTitle: UITextField!
    var pet:Pet?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            textName.becomeFirstResponder()
        }
        if indexPath.section == 1 {
            textName.becomeFirstResponder()
        }
    }
    
    //unwind segue
    // SavePetDetailWithSegue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SavePetDetailWithSegue" {
            self.pet = Pet(name:self.textName.text ?? "Default name", title:self.textTitle.text ?? "Default title")
        }
    }

}
