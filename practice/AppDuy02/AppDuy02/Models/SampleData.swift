//
//  SampleData.swift
//  AppDuy02
//
//  Created by duybui on 11/2/17.
//  Copyright © 2017 duybui. All rights reserved.
//

import Foundation
let petsData = [
    Pet(name: "Dog", title: "This is dog"),
    Pet(name: "Cat", title: "This is cat"),
    Pet(name: "Bird", title: "This is bird")
]
